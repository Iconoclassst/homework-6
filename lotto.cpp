// Nory Lazo
// CSIS 135 TTh 510pm
// Homework 6

//preporcessor directives
#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
using namespace std;

//fucntion prototypes
void getLottoPicks(int array[]);
void genWinNums(int array[]);
bool noDuplicate(int array[], int, int);


int main()
{
	srand((unsigned int)time(NULL));
	string selection; //for menu selection
	string userName; //for username
	string winnings; //diplay what they won

	int UserTicket[7]; //array for users ticket numbers
	int WinningNum[7]; //array for winning numbers
	int match = 0; //to check the UserTicket with WinningNum

	do //lets the user play lotto until they quit
	{
		//user menu
		cout << "LITTLETON CITY LOTTO MODEL:" << endl
			 << "---------------------------" << endl
			 << "1) Play Lotto" << endl
			 << "q) Quit Program" << endl
			 << "Please make a selection: ";
		getline(cin, selection);

		//input validation
		while (selection != "1" && selection != "q" && selection != "Q")
		{
			cout << "Invalid selection. Please try again.";
			getline(cin, selection);
		}
	
		if (selection == "1")
		{
			cout << "Please enter your name: ";
			getline(cin, userName);

			getLottoPicks(UserTicket); //calls function, saves the user input into UserTicket array

			genWinNums(WinningNum); //calls function, generates random numbers, stores it into WinningNum array

			//adds how many matching numbers they got
			for (int i = 0; i < 7; i++)
			{
				if (UserTicket[i] == WinningNum[i])
				{
					match++;
				}
			}

			if (match == 7)
			{
				winnings = "JACKPOT - 1 MILLION";
			}
			else if (match == 6)
			{
				winnings = "GREAT! - $100,000";
			}
			else if (match == 5)
			{
				winnings = " LUCKY YOU! - $5,000";
			}
			else if (match == 4)
			{
				winnings = "NOT BAD - $100";
			}
			else if (match == 3)
			{
				winnings = "FREE TICKET";
			}
			else if (match <= 2)
			{
				winnings = "SORRY NOTHING";
			}

			cout << endl;
			cout << userName << "'S LOTTO RESULTS" << endl
			 << "----------------------" << endl
			 << "WINNING TICKET NUMBERS : ";
			
			for (int i = 0; i < 7; i++)
			{
				cout << WinningNum[i];
				cout << "  ";
			}

			cout << endl
				 << userName << "'S TICKET         : ";

			for (int i = 0; i < 7; i++)
			{
				cout << UserTicket[i];
				cout << "  ";
			}
			cout << endl << endl;

			cout << "RESULTS :" << endl
				 << "---------" << endl
				 << "Number Matches: " << match << endl
				 << "Winnings      : " << winnings << endl << endl;

		}
		else if (selection == "q" || selection == "Q")
		{
			cout << "You have chosen to quit the program. Thank you for using!" << endl;
		}

	}while (selection != "q" && selection != "Q");	

	system("pause");
	return 0;
}

void getLottoPicks(int array[])
{
	cout << "Please enter your 7 lotto number picks between 1 and 40." << endl;

	for (int count = 0; count < 7; count++)
	{
		cout << "selection #" << count + 1 << ": ";
		cin >> array[count];

		while (array[count] < 1 || array[count] > 40)
		{
			cout << "The number must be between 1 and 40. Please try again: ";
			cin >> array[count];
		}

		while (noDuplicate(array, count, array[count]))
		{
			cout << "You already picked this number. Please enter a different number: ";
			cin >> array[count];
		}
	}
}

void genWinNums(int array[])
{
	for (int count = 0; count < 7; count++)
	{
		array[count] = rand() % 40 + 1;

		while (noDuplicate(array, count, array[count]))
		{
			array[count] = rand() % 40 + 1;
		}
	}
}

bool noDuplicate(int array[], int arrSize, int number)
{
	bool duplicate = 0;

	for (int i = 0; i < arrSize; i++)
	{
		if (array[i] == number)
		{
			duplicate = 1;
		}
	}
	return duplicate;
}